﻿#include <iostream>

int main()
{
    int a = 0;
    static int N = 90;

    for (int i = 0; i < N; ++i)
    {
        // Вывод всех четных значений
        /*if (a % 2 == 0)
        {
            std::cout << "\n" << a;
        }*/

        //Вывод всех нечетных значений
        if (a % 2 != 0)
        {
            std::cout << a << "\n";
        }

        a++;
    }
}